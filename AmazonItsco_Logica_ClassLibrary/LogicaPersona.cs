﻿using AmazonItsco_Data_ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonItsco_Logica_ClassLibrary
{
    public class LogicaPersona
    {
        private static dcAmazonItscoDataContext dc = new dcAmazonItscoDataContext();

        public static List<Persona> getAllPerson()
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A"));

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }
        }

        public static Persona getAllPersonByID(int codigo)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")).FirstOrDefault();

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }
        }

        public static Persona getPersonByID(int codigo)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")
                                                && data.per_id.Equals(codigo)).FirstOrDefault();

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }
        }

        public static List<Persona> getAllPersonByDni(string cedula)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")
                                                && data.per_dni.StartsWith(cedula));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas" + ex.Message);
            }
        }

        public static List<Persona> getAllPersonByApellidos(string apellidos)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")
                                                && data.per_apellidos.StartsWith(apellidos));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas por apellidos" + ex.Message);
            }
        }

        public static List<Persona> getAllPersonByNombres(string nombres)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")
                                                && data.per_nombre.StartsWith(nombres));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas por nombres" + ex.Message);
            }
        }

        public static List<Persona> getAllPersonByGenero(string genero)
        {
            try
            {
                var resUser = dc.Personas.Where(data => data.per_status.Equals("A")
                                                && data.per_genero.Equals(genero));

                //SELECT * FROM Persona WHERE per_status='A' and per_dni LIKE 'cedula%';

                return resUser.ToList();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener personas por nombres" + ex.Message);
            }
        }

        public static bool savePersona(Persona persona)
        {
            try
            {
                bool res = false;
                persona.per_status = 'A';
                persona.per_add = DateTime.Now;
                persona.per_edit = DateTime.Now;
                persona.per_delete = DateTime.Now;

                dc.Personas.InsertOnSubmit(persona);

                //actualizar BD o contexto de datos commit.
                dc.SubmitChanges();

                res = true;
                return res;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al insertar personas" + ex.Message);
            }
        }

        public static bool updatePersona(Persona persona)
        {
            try
            {
                bool res = false;
                persona.per_edit = DateTime.Now;
                //actualizar BD o contexto de datos commit.
                dc.SubmitChanges();

                res = true;
                return res;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al actualizar personas" + ex.Message);
            }
        }


        public static bool deletePersona(Persona persona)
        {
            try
            {
                bool res = false;
                persona.per_delete = DateTime.Now;
                persona.per_status = 'I';
                //actualizar BD o contexto de datos commit.
                dc.SubmitChanges();

                res = true;
                return res;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al eliminar personas" + ex.Message);
            }
        }


    }
}
