﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmazonItsco_Data_ClassLibrary;
using System.Security.Cryptography;

namespace AmazonItsco_Logica_ClassLibrary
{
    public class LogicaUsuario
    {
        private static dcAmazonItscoDataContext dc = new dcAmazonItscoDataContext();

        public static Usuario getUserbyUsernameAndPassword(string correo, string clave)
        {
            //Seleccionar codigo y presionar Ctrl + K +S para ponerlo dentro de un try o if
            try
            {
                var resUser = dc.Usuarios.Where(data => data.usu_correo.Equals(correo)
                                                     && data.usu_clave.Equals(clave)
                                                     && data.usu_status.Equals("A")).
                                              FirstOrDefault();

            //Select * from Usuario where usu_correo='correo' and
            //usu_clave='clave'
            //usu_status='A'
                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener usuario" + ex.Message);
            }
        }

        public static Usuario getUserbyCorreo(string correo)
        {
            //Seleccionar codigo y presionar Ctrl + K +S para ponerlo dentro de un try o if
            try
            {
                var resUser = dc.Usuarios.Where(data => data.usu_correo.Equals(correo)
                                                     && data.usu_status.Equals("A")).
                                              FirstOrDefault();

                return resUser;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener usuario" + ex.Message);
            }
        }

        public static bool updateUsuario(Usuario usuario)
        {
            try
            {
                bool res = false;
                usuario.usu_edit = DateTime.Now;
                //actualizar BD o contexto de datos commit.
                dc.SubmitChanges();

                res = true;
                return res;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al actualizar usuario" + ex.Message);
            }
        }

        public static bool blockUsuario(Usuario usuario)
        {  

            try
            {
                bool resUsu = false;
                usuario.usu_edit = DateTime.Now;
                usuario.usu_status = 'B';
                dc.SubmitChanges();

                resUsu = true;
                return resUsu;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al actualizar usuario" + ex.Message);
            }
            
            
            
        }

        public static bool intUsuario(Usuario usuario, int contador)
        {
            
            try
            {
                bool resUsu = false;
                usuario.usu_edit = DateTime.Now;
                usuario.per_intentos = (byte?)contador;
                dc.SubmitChanges();

                resUsu = true;
                return resUsu;
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al actualizar usuario" + ex.Message);
            }


        }

    }
}
