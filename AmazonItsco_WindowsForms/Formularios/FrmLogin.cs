﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmazonItsco_Data_ClassLibrary;
using AmazonItsco_Logica_ClassLibrary;

namespace AmazonItsco_WindowsForms.Formularios
{
    public partial class FrmLogin : Form
    {
        int contador = 0;

        public FrmLogin()
        {
            InitializeComponent();
        }

        
        private void login()
        {
            string correo = txtCorreo.Text.TrimEnd();
            string clave = txtClave.Text.TrimEnd();



            if (!string.IsNullOrEmpty(correo) && !string.IsNullOrEmpty(clave))
            {
                Usuario usuario = new Usuario();
                usuario = LogicaUsuario.getUserbyUsernameAndPassword(correo, clave);
                

                if (usuario != null)
                {
                    MessageBox.Show("Bienvenido al sistema.\n" + usuario.Perfil.pef_descripcion + "\n" +
                        " " + usuario.Persona.per_apellidos, "Sistema AmazonItsco", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    FrmMaster frmMaster = new FrmMaster();
                    frmMaster.Show();
                    this.Hide();
                }
                else
                {
                    contador = contador +1;

                    MessageBox.Show("Correo o clave incorrecta. Intento "+contador+" de 3" ,"Sistema AmazonItsco", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    intentosUser(correo, contador);
                   
                    if (contador >= 3)
                    {
                        bloquearUser(correo);    
                    }

                } 
                
            }

        }

        private void bloquearUser(string correo)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario = LogicaUsuario.getUserbyCorreo(correo);

                bool res = LogicaUsuario.blockUsuario(usuario);
                if (true)
                {
                    
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Usuario bloqueado", "Sistema AmazonItsco", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void intentosUser(string correo, int intentos)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario = LogicaUsuario.getUserbyCorreo(correo);

                bool res = LogicaUsuario.intUsuario(usuario, intentos);
                if (true)
                {
                   
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Usuario bloqueado", "Sistema AmazonItsco", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            login();
        }
    }
}
