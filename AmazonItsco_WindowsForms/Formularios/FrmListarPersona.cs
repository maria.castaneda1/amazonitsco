﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmazonItsco_Data_ClassLibrary;
using AmazonItsco_Logica_ClassLibrary;

namespace AmazonItsco_WindowsForms.Formularios
{
    public partial class FrmListarPersona : Form
    {
        public FrmListarPersona()
        {
            InitializeComponent();
        }

        private void loadPerson(List<Persona> listaPersonas)
        {
            //var listaPersonas = LogicaPersona.getAllPerson(); // llamamos a la data
            if (listaPersonas.Count >0 && listaPersonas !=null) //verificamos que la coleccion tenga datos
            {
                dgvPersonas.DataSource = listaPersonas.Select(data => new  //asignamos a la propiedad data source, para llenar combo box
                //list item, todo lo que tenga que ver con base de datos para llenar 
                // usamos LINQ para generar una nueva lista y seleccionar los campos que necesito.
                {
                    CODIGO = data.per_id,
                    APELLIDOS = data.per_apellidos,
                    NOMBRES = data.per_nombre,
                    IDENTIFICACION = data.per_dni,
                    TIPO = data.per_tipodni,
                    GENERO = data.per_genero,

                }).ToList(); // agregar el toList al final para cargar la nueva lista
            }

        }

        private void FrmListarPersona_Load(object sender, EventArgs e)
        {
            var listaPersonas = LogicaPersona.getAllPerson();
            loadPerson(listaPersonas);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarPersona(cmbTipoBuscar.Text);
        }

        private void buscarPersona(string op)
        {
            List<Persona> listaPersona = new List<Persona>();
            string dataABuscar = txtBuscar.Text.TrimEnd();

            if (!string.IsNullOrEmpty(dataABuscar))
            {
                switch (op)
                {
                    case "Cedula":
                        listaPersona = LogicaPersona.getAllPersonByDni(dataABuscar);
                        loadPerson(listaPersona);
                        break;

                    case "Apellidos":
                        listaPersona = LogicaPersona.getAllPersonByApellidos(dataABuscar);
                        loadPerson(listaPersona);
                        break;

                    case "Nombres":
                        listaPersona = LogicaPersona.getAllPersonByNombres(dataABuscar);
                        loadPerson(listaPersona);
                        break;

                    case "Genero":
                        listaPersona = LogicaPersona.getAllPersonByGenero(dataABuscar);
                        loadPerson(listaPersona);
                        break;
                }
            }
            else if (string.IsNullOrEmpty(dataABuscar))
            {
                switch (op)
                {
                    case "Todos":
                        listaPersona = LogicaPersona.getAllPerson();
                        loadPerson(listaPersona);
                        break;
                }
            }      
        }
    }
}
